import React from 'react'
import { ScrollView, StyleSheet, View } from 'react-native'
import { Header, CoffeeCard } from '../components'

export class CoffeTypesScreen extends React.Component {
    static navigationOptions = {
        header: null
    };

    render() {
        return (
            <View style={{backgroundColor: '#f9f5f0'}}>
                <Header title={'Coffee Types'} />
                <ScrollView>
                    <View style={styles.container}>
                        <CoffeeCard item={
                            {
                                title: 'AMERICAN LATTE',
                                imageLink: require('../assets/images/coffee/image_01.png')
                            }
                        } />
                        <CoffeeCard item={
                            {
                                title: 'AMERICAN LATTE',
                                imageLink: require('../assets/images/coffee/image_02.png')
                            }
                        } />
                        <CoffeeCard item={
                            {
                                title: 'AMERICAN LATTE',
                                imageLink: require('../assets/images/coffee/image_03.png')
                            }
                        } />

                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        flexDirection: 'row',
        flexWrap: 'wrap',
        flexShrink: 2,
        justifyContent: 'space-around',
        marginBottom: 130
    },
});
