import React from 'react'
import {
  StyleSheet,
  View,
  Text,
  Image
} from 'react-native'
import { Header } from '../components'

export class HomeScreen extends React.Component {

  static navigationOptions = {
    header: null,
  }

  render() {
    return (
      <View style={styles.container}>
        <Header title={'Hel\'loo, Pti4ka...'} />
        <View style={styles.imageContainer}>
          <Image
            source={require('../assets/images/layer2.png')}
            style={styles.welcomeImage}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f9f5f0'
  },
  headerContainer: {
    height: 90,
    justifyContent: 'center',
    backgroundColor: '#de3b43',
    paddingLeft: 25,
    paddingTop: 30,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    elevation: 2,
    position: 'relative'
  },
  textHeaderContainer: {
    color: '#fff',
    fontSize: 28,
    fontFamily: 'AvenirNext-DemiBold'
  },
  imageContainer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center'
  },
  welcomeImage: {
    resizeMode: 'contain'
  }
})
